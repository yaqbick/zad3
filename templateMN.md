---
author: Michał Jakubowski
title: Moduł spotkań B2B
subtitle: zarys projektu
date: 
theme: Lublin
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Moduł B2B

### Formidable

Najprościej mówiąc  - jest to moduł, który pozwala administratorowi na tworzenie nowych formularzy na wydarzenia i automatyczne „parowanie” uczestników w oparciu o wybrane kryteria (na przykład branżę czy język spotkania). Taki trochę „business tinder”, gdyż służy do umawiania uczestników na spotkania biznesowe w czasie danej konferencji.  Moduł jest dodatkowo rozbudowany o mały system mailingowy w celu kontaktu z wybranymi uczestnikami. 

Moduł opiera się na dwóch pluginach:

* **Formidable** – wtyczka do generowania formularzy
* **Matching B2B** – właściwy moduł, udostępniający panele dla administratora, z poziomu których może tworzyć listy sparowanych uczestników i wysyłać do nich maila

**Jeśli  wtyczka Formidable będzie nieaktywna, to Matching B2B nie zadziała!**

*Zacznijmy od Formidable.*
<!--- Kolorowanie tekstu wymaga użycia komend Latexa -->
\textcolor{red}{Samo tworzenie i dodawanie formularzy jest proste}, \textcolor{blue}{więc nie ma co się specjalnie rozwodzić}
<!--- Centrowanie wymaga użycia komend Latexa -->

\begin{center}Nas interesuje, co jest „pod maską”. Zwróćmy uwagę na zaawansowane paramtery każdego pola formularza, a konkretnie na „Klucz pola \end{center}

![alt text](formidable.png "Formidable")

Formidable automatycznie generuje własny unikalny klucz, który w bazie danych jest jedynym – poza id – identyfikatorem danego pola.

## Matching B2B

Możemy teraz przejść do omówienia najważniejszego elementu. Struktura wtyczki wygląda następująco:
<!--- UWAGA WAŻNE pusta linia odstępu -->

1. **classes** – klasy dedykowane do poszczególnych zadań
2. **controllers** – kontrolery do sterowania serwisami i wyświetlania widoków
3. **interface** – tak żeby była jakaś namiastka SOLID-u 😉
4. **services** – główne źródło niemal całej logiki biznesowej
5. **templates** – szablony, głównie wiadomości email
6. **views**  - widoki, głównie dla administatora strony
7. **Run.php** – „serce” business tindera


Najlepiej będzie zacząć od omówienia serwisów. Są one następujące:

* **FormService** – serwis zintegrowany z wtyczką Formidable, który pobiera, grupuje, filtruje dane z bazy formularzy
* **MailService** – serwis systemu mailowego
* **MatchingService** – serwis, który zapisuje wyniki parowania w tabeli wp_matching i w razie potrzeby wyświetla je wraz z „dołączonymi” danymi z bazy formularzy(imię, nazwisko, firma)
* **DbService** – obsługuje zapytania do bazy wp_matching
* **RequestService** – dość amatorskie podejśćie do przechwytywania zapytań POST, ale chodziło o to, żeby wszystko było w jednym miejscu, a nie co chwilę if(isset($_POST) wszędzie w kodzie.

Serwisy obsługują poniższy panel

![alt text](panel.png "panel")


Gdy Administrator kliknie na przycisk “generuj nową listę”, panelController uruchamia “tindera”, którego serce znajduje się w pliku run.php. Proces przebiega w kilku etapach:

+ FormService tworzy listę wszystkich uczestników z danego formularza, którzy wyrazili zgodę na spotkania B2B. Metoda: getFormTinderDa()
    - funkcja iteruje po wszystkich “wejściach”(entry) z danego formularza 
    - na bazie każdego wejścia powstaje obiekt Participant
    - każdy obiekt Participant zostaje uzupełniony o kryteria parowania: przedział czasowy, wybrane kryteria(np. branża, język spotkania) oraz porządane kryteria spotkania(np. branża, język spotkania) 
    - UWAGA! Na tym etapie są sprawdzane klucze pól, o których była mowa na początku. Jeśli któryś klucz zostanie pominięty lub będzie źle zapisany może nastąpić błąd.
    - zostaje zwrócona lista obiektów Participant, gotowa do parowania
+ run.php zaczyna iterować po uczestnikach i tworzy listę spotkań na podstawie kryteriów parowania
+ panelController usuwa z tabeli wp_matching istniejącą listę par dla danego formularza i zapisuje nową. 

## System Mailingowy

### Moduły
Dodaj Email

###
Edytuj Email

###
Wyślij Email

<!--- Niestety pomiędzy tak zdefiniowanymi  blokami nie można umieszczać innych elementów-->

## Etapy wysyłania wiadomości Email

\begin{alertblock}{Etap 1}
RequestService rozpoznaje żądanie, pobiera z niego id formularza, wybrany szablon, tytuł i grupę docelową, a następnie uruchamia metody z  MailService
\end{alertblock}

MailService:

\begin{block}{grupowanie danych}
dobiera właściwą grupę docelową i agreguje wszystkie dane w tabeli obiektów typu MailRecipent (MailRecipent.php) przy pomocy metody aggregate()
\end{block}

\begin{exampleblock}{iteracja}
iteruje po wszystkich obiektach MailrRecipent i dla każdego uruchamia metodę: sendManual()
\end{exampleblock}

\begin{exampleblock}{parsowanie}
treść maila zostaje przepuszczona przez Parser, który zamienia tagi na imię, nazwisko i listę spotkań danego uczestnika z listy
\end{exampleblock}

\begin{exampleblock}{wysyłanie}
mail zostaje wysłany 
\end{exampleblock}

## Fragmenty kodu

Fragment klasy MailRecipent:
```public function getNameByEmail($email)
    {
        $formService = new FormService();
        $participant =  $formService->getTinderParticipantByEmail($email, $this->formID);
        if ($participant) {
            return $participant->getName();
        }
    }
    public function getSurnameByEmail($email)
    {
        $formService = new FormService();
        $participant = $formService->getTinderParticipantByEmail($email, $this->formID);
        if ($participant) {
            return $participant->getSurname();
        }
    }
```


## Slajd 6 - - umieszczanie kodu źródłowego Latex {.fragile}

Kod wbudowany
\begin{lstlisting}[language=c, basicstyle=\scriptsize\ttfamily,identifierstyle=\color{blue},]
int a=1;
for (int i=3;i<10;++i);
\end{lstlisting}
Kod z pliku
\lstinputlisting[language=c, linerange={2-4}]{src/first.c}
## Istotne klucze pól formularza

::: {.columns}
:::: {.column width=0.3}
* myy
* myz
* mychoicey
* mychoicez
* timeinterval

::::
:::: {.column width=0.3}

::::
:::: {.column width=0.3}
* name
* surname
* company
* agreement

::::
:::

## Edycja Formularzy

![alt text](form.png "panel")

## Tabela sparowanych uczestników

| participant A | participantB  | timeinterval   | formId  |
| ------------- |:-------------:| :------------: |--------:| 
| yaqbick@wp.pl | mwoa@gmail.com|   11:00        |    3    |
| aaa@gmail.com | bbb@gmail.com |   11:00        |    3    |
| aaa@gmail.com | ccc@gmail.com |   11:00        |    3    |
| ccc@gmail.com | ddd@gmail.com |   11:00        |    3    |
| ddd@gmail.com | bbb@gmail.com |   11:00        |    3    |


<!--- W kodzie można dowolnie używać Latexa -->

## Slajd 11 - wzory

<!--- wzory wstawiamy bezpośrednio jako jod Latex -->

Tu jest tekst ze wzorem: $c=\sqrt{a^2+b^2}$

A tu wzór pod tekstem: $$c=\sqrt{a^2+b^2}$$